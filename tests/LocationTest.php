<?php

namespace App\Tests;

use App\Entity\Location;
use PHPUnit\Framework\TestCase;

class LocationTest extends TestCase
{
    public function testLocation()
    {
        $location = new Location();
        $location->setPrix(100.50);

        $this->assertEquals(100.50, $location->getPrix());
    }
}
