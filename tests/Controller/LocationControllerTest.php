<?php

namespace App\Test\Controller;

use App\Entity\Location;
use App\Repository\LocationRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LocationControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private LocationRepository $repository;
    private string $path = '/location/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->repository = (static::getContainer()->get('doctrine'))->getRepository(Location::class);

        foreach ($this->repository->findAll() as $object) {
            $this->repository->remove($object, true);
        }
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Location index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $originalNumObjectsInRepository = count($this->repository->findAll());

        $this->markTestIncomplete();
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'location[dateD]' => 'Testing',
            'location[dateA]' => 'Testing',
            'location[prix]' => 'Testing',
            'location[voiture]' => 'Testing',
            'location[client]' => 'Testing',
        ]);

        self::assertResponseRedirects('/location/');

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));
    }

    public function testShow(): void
    {
        $this->markTestIncomplete();
        $fixture = new Location();
        $fixture->setDateD('My Title');
        $fixture->setDateA('My Title');
        $fixture->setPrix('My Title');
        $fixture->setVoiture('My Title');
        $fixture->setClient('My Title');

        $this->repository->add($fixture, true);

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Location');

        // Use assertions to check that the properties are properly displayed.
    }

    public function testEdit(): void
    {
        $this->markTestIncomplete();
        $fixture = new Location();
        $fixture->setDateD('My Title');
        $fixture->setDateA('My Title');
        $fixture->setPrix('My Title');
        $fixture->setVoiture('My Title');
        $fixture->setClient('My Title');

        $this->repository->add($fixture, true);

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'location[dateD]' => 'Something New',
            'location[dateA]' => 'Something New',
            'location[prix]' => 'Something New',
            'location[voiture]' => 'Something New',
            'location[client]' => 'Something New',
        ]);

        self::assertResponseRedirects('/location/');

        $fixture = $this->repository->findAll();

        self::assertSame('Something New', $fixture[0]->getDateD());
        self::assertSame('Something New', $fixture[0]->getDateA());
        self::assertSame('Something New', $fixture[0]->getPrix());
        self::assertSame('Something New', $fixture[0]->getVoiture());
        self::assertSame('Something New', $fixture[0]->getClient());
    }

    public function testRemove(): void
    {
        $this->markTestIncomplete();

        $originalNumObjectsInRepository = count($this->repository->findAll());

        $fixture = new Location();
        $fixture->setDateD('My Title');
        $fixture->setDateA('My Title');
        $fixture->setPrix('My Title');
        $fixture->setVoiture('My Title');
        $fixture->setClient('My Title');

        $this->repository->add($fixture, true);

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertSame($originalNumObjectsInRepository, count($this->repository->findAll()));
        self::assertResponseRedirects('/location/');
    }
}
