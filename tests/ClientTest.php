<?php

namespace App\Tests;

use App\Entity\Client;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    public function testClient()
    {
        $client = new Client();
        $client->setNom('John');
        $client->setPrenom('Doe');

        $this->assertEquals('John', $client->getNom());
        $this->assertEquals('Doe', $client->getPrenom());
    }
}
